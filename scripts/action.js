window.ampActions = {
   prefillWithProfile: 'prefillWithProfile',
   getProfiles: 'getProfiles',
   getProfilesResponse: 'getProfilesResponse',
   removeProfile: 'removeProfile',
   addProfile: 'addProfile',
   importProfiles: 'importProfiles',
   changeArrayWithoutReload: 'changeArrayWithoutReload',
   changeArrayWithReload: 'changeArrayWithReload',
   changeMenuMode: 'changeMenuMode',
   getProfilesFromKeyWatcher: 'getProfilesFromKeyWatcher',
   getProfilesFromKeyWatcherResponse: 'getProfilesFromKeyWatcherResponse',
   refreshExtMenus: 'refreshExtMenus',
};

