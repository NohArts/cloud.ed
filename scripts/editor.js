$ = jQuery;

$.fn.moveUp = function() {
    before = $(this).prev();
    $(this).insertBefore(before);
}

$.fn.moveDown = function() {
    after = $(this).next();
    $(this).insertAfter(after);
}

var UI = {
    templates: {
        view: _.template($('#profile-view').html()),
        form: _.template($('#profile-form').html()),
    },
};

$(function  () {
    initStorage();

    if (isAPIAvailable()) {
        $('nav .import-csv').bind('change', importProfiles);
    } else {
        $('nav .import-csv').hide();
    }

    $("nav a.export-csv").click(exportProfiles);
    $("nav a.new-profile").click(createProfile);

    $("#formProfile .modal-footer .btn-success").click(saveProfile);

    $('#showBy').change(function() {
        localStorage['AmazonMerchListing:v0.1'] = JSON.stringify($(this).prop('checked'));

        sendMessage('refreshExtMenus', readProfiles());
    });
});

function removeProfile (ev) {
    bootbox.confirm("Are you sure you want to delete ?", function (answer) {
        if (answer) {
            $(ev.target).parents('li[data-id]').remove();

            saveStorage();
        }
    });
}

function renderProfile (uid) {
    var $target = $('.profiles-list li[data-id="'+uid+'"]');

    $target.find('input[type="checkbox"]').bootstrapToggle({
        size: 'small',
    }).on('change',toggleProfile);

    $target.find('.edit-profile').click(editProfile);
    $target.find('.remove-profile').click(removeProfile);

    if ($target.prev().length) {
        $target.find(".move-up").click(function (ev) {
            $(ev.target).parents('li').moveUp();

            saveStorage();
        });
    } else {
        $target.find(".move-up").off('click');
    }

    if ($target.next().length) {
        $target.find(".move-down").click(function (ev) {
            $(ev.target).parents('li').moveDown();

            saveStorage();
        });
    } else {
        $target.find(".move-down").off('click');
    }
}

function toggleProfile (ev) {
    var uid = parseInt($(ev.target).parents('li[data-id]').attr('data-id'));

    var data = JSON.parse(localStorage['AmazonMerchProfiles:v0.1']);

    for (var i=0 ; i<data.length ; i++) {
        if (data[i].uid==uid) {
            data[i].enabled = $(ev.target).prop('checked');
        }
    }

    writeStorage(data);
}

function getProfile (uid) {
    var data = JSON.parse(localStorage['AmazonMerchProfiles:v0.1']);

    for (var i=0 ; i<data.length ; i++) {
        if (data[i].uid==uid) {
            return data[i];
        }
    }

    return null;
}

function editProfile (ev) {
    var uid = parseInt($(ev.target).parents('li[data-id]').attr('data-id'));

    var data = getProfile(uid);

    if (data!=null) {
        var dialog = bootbox.dialog({
            title: 'Create a new Profile',
            message: UI.templates.form({
                entry:  data,
            }),
            buttons: {
                confirm: {
                    label: "Save",
                    className: 'btn-success',
                    callback: function() {
                        var item = deduceProfile('#formProfile');

                        resp = UI.templates.view({
                            entry:  item,
                        });

                        $('.profiles-list li[data-id="'+item.uid+'"]').replaceWith(resp);

                        renderProfile(item.uid);

                        saveStorage();
                    }
                },
                cancel: {
                    label: "Cancel",
                    className: 'btn-danger',
                },
            }
        }).on('shown.bs.modal', function (ev) {
            $('#formProfile input[type="checkbox"]').bootstrapToggle({
                size: 'small',
            });
        });
    }
}

function deduceProfile (target) {
    var item = {
        uid:       $(target+' *[name="index-field"]').val(),
        brandName: $(target+' *[name="brand-field"]').val(),
        title:     $(target+' *[name="title-field"]').val(),
        shortcut:  $(target+' *[name="shortcut-field"]').val(),
        price:     $(target+' *[name="price-field"]').val(),
        features:  [
            $(target+' *[name="feature-one-field"]').val(),
            $(target+' *[name="feature-two-field"]').val(),
        ],
        description: $(target+' *[name="description-field"]').val(),
        enabled: $(target+' *[name="enabled-field"]').prop('checked'),
    };

    item.uid     = parseInt(item.uid);
    item.price   = parseFloat(item.price.replace(',','.'));

    return item;
}

function saveProfile (ev) {
    var item = deduceProfile('#formProfile');

    resp = UI.templates.view({
        entry:  item,
    });

    $('.profiles-list li[data-id="'+item.uid+'"]').replaceWith(resp);

    $("#formProfile").modal('hide');

    renderProfile(item.uid);

    saveStorage();
}

function createProfile (ev) {
    var data = {
        uid:       $(".profiles-list li").length,
        brandName: "",
        title:     "",
        shortcut:  "",
        price:     "",
        features:  [
            "",
            "",
        ],
        description: "",
        enabled:   false,
    };

    var dialog = bootbox.dialog({
        title: 'Create a new Profile',
        message: UI.templates.form({
            entry:  data,
        }),
        buttons: {
            confirm: {
                label: "Save",
                className: 'btn-success',
                callback: function() {
                    var item = deduceProfile('#formProfile');

                    resp = UI.templates.view({
                        entry:  item,
                    });

                    $('.profiles-list').append(resp);

                    renderProfile(item.uid);

                    saveStorage();
                }
            },
            cancel: {
                label: "Cancel",
                className: 'btn-danger',
            },
        }
    }).on('shown.bs.modal', function (ev) {
        $('#formProfile input[type="checkbox"]').bootstrapToggle({
            size: 'small',
        });
    });
}

function initStorage () {
    var data = localStorage['AmazonMerchProfiles:v0.1'];

    if (data=='undefined' || data==undefined) {
        localStorage['AmazonMerchProfiles:v0.1'] = '[]';
    }

    var flag = localStorage['AmazonMerchListing:v0.1'];

    if (flag=='undefined' || flag==undefined) {
        localStorage['AmazonMerchListing:v0.1'] = 'false';

        flag = localStorage['AmazonMerchListing:v0.1'];
    }

    loadStorage();

    $('#showBy').prop('checked',JSON.parse(flag)).change();
}

function loadStorage () {
    var data = localStorage['AmazonMerchProfiles:v0.1'];

    var data = JSON.parse(localStorage['AmazonMerchProfiles:v0.1']);

    $(".profiles-list").empty();

    for (var i=0 ; i<data.length ; i++) {
        $(".profiles-list").append(UI.templates.view({
            entry:  data[i],
        }));
    }

    for (var i=0 ; i<data.length ; i++) {
        renderProfile(data[i].uid);
    }
}

function readProfiles () {
    var lst = $('.profiles-list li[data-id]');

    var resp=[], item, $elem;

    for (var i=0 ; i<lst.length ; i++) {
        $elem = $(lst[i]);

        $feat = $elem.find(".field-feature");

        item = {
            uid:       $elem.attr('data-id'),
            brandName: $elem.find(".field-brand").text(),
            title:     $elem.find(".field-title").text(),

            price:     $elem.find(".field-price").text(),
            features:  [],
            description: $elem.find(".field-description").text(),

            shortcut:  $elem.find(".field-shortcut").text(),
            enabled:   $elem.find(".field-enabled").prop('checked'),
        };

        for (var j=0 ; j<$feat.length ; j++) {
            value = $feat[j].innerText || "";

            if (value.trim().length!=0) {
                item.features.push(value);
            }
        }

        item.price   = parseFloat(item.price.replace('$','').replace(',','.'));

        resp.push(item);
    }

    return resp;
}

function saveStorage () {
    var resp = readProfiles();

    writeStorage(resp);

    var flag = $('#showBy').prop('checked');

    localStorage["AmazonMerchListing:v0.1"] = JSON.stringify(flag);
}

function sendMessage (alias, payload) {
    if (typeof chrome !== 'undefined') {
        var topic = window.ampActions[alias];

        if (typeof topic !== 'undefined') {
            chrome.runtime.sendMessage({
                action: topic,
                data: payload,
            });
        }
    }
}

function writeStorage (resp) {
    localStorage["AmazonMerchProfiles:v0.1"] = JSON.stringify(resp);

    sendMessage('refreshExtMenus', resp);

    loadStorage();
}

function exportProfiles (ev) {
    var orig = readProfiles(), resp = [
        ['Brand name', 'Title', 'Price', 'Feature 1', 'Feature 2', 'Description', 'Shortcut'],
    ];

    for (var i=0 ; i<orig.length ; i++) {
        item = [
            orig[i].brandName,
            orig[i].title,
            orig[i].price,
        ];

        item.push((0<orig[i].features.length)?orig[i].features[0]:'');
        item.push((1<orig[i].features.length)?orig[i].features[1]:'');

        item.push(orig[i].description);
        item.push(orig[i].shortcut);

        resp.push(item);
    }

    downloadFileFromText('amazon-merch-profiles.csv', $.csv.fromArrays(resp));
}

function downloadFileFromText(filename, content) {
    var a = document.createElement('a');
    var blob = new Blob([ content ], {type : "text/plain;charset=UTF-8"});
    a.href = window.URL.createObjectURL(blob);
    a.download = filename;
    a.style.display = 'none';
    document.body.appendChild(a);
    a.click();
    delete a;
}

function importProfiles(evt) {
    var files = evt.target.files;
    var file = files[0];

    var output = ''
        output += '<span style="font-weight:bold;">' + escape(file.name) + '</span><br />\n';
        output += ' - FileType: ' + (file.type || 'n/a') + '<br />\n';
        output += ' - FileSize: ' + file.size + ' bytes<br />\n';
        output += ' - LastModified: ' + (file.lastModifiedDate ? file.lastModifiedDate.toLocaleDateString() : 'n/a') + '<br />\n';

    var reader = new FileReader();
    reader.readAsText(file);
    reader.onload = function(event){
        var csv = event.target.result;
        var data = $.csv.toArrays(csv);
        var html = '';
        var meta = null;

        var resp = readProfiles();

        for (var i = 1; i < data.length; i++) {
            var row = data[i];
            var entry = {};

            entry.uid = i-1;
            entry.enabled = true;
            entry.profileId = i + 1;

            entry.brandName = row[0];
            entry.title = row[1];
            entry.price = row[2];
            entry.features = [];

            if (row.length > 3) {
                entry.features.push(row[3]);
            }
            if (row.length > 4) {
                entry.features.push(row[4]);
            }
            if (row.length > 5) {
                entry.description = row[5];
            }
            if (row.length > 6) {
                entry.shortcut = row[6];
            }

            resp.push(entry);

            $(".profiles-list").append(UI.templates.view({
                entry: entry,
            }));

            renderProfile(entry.uid);
        }

        writeStorage(resp);
    };
    reader.onerror = function(){ alert('Unable to read ' + file.fileName); };
}

function isAPIAvailable() {
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        return true;
    } else {
        document.writeln('The HTML5 APIs used in this form are only available in the following browsers:<br />');
        document.writeln(' - Google Chrome: 13.0 or later<br />');
        document.writeln(' - Mozilla Firefox: 6.0 or later<br />');
        document.writeln(' - Internet Explorer: Not supported (partial support expected in 10.0)<br />');
        document.writeln(' - Safari: Not supported<br />');
        document.writeln(' - Opera: Not supported');
        return false;
    }
}
