console.log("Ok, AutobahnJS loaded", autobahn.version);

var principal = "joe";
var ticket = "secret!!!";

var connection = new autobahn.Connection({
    url: 'ws://neuro-chip.herokuapp.com/spine',
    realm: 'uchikoma',

    authmethods: ["ticket"],
    authid: principal,
    onchallenge: function (session, method, extra) {
       console.log("onchallenge", method, extra);
       if (method === "ticket") {
          return ticket;
       } else {
          throw "don't know how to authenticate using '" + method + "'";
       }
    }
});

connection.onopen = function (session,details) {
    console.log("connected session with ID " + session.id);
    console.log("authenticated using method '" + details.authmethod + "' and provider '" + details.authprovider + "'");
    console.log("authenticated with authid '" + details.authid + "' and authrole '" + details.authrole + "'");

   // 1) subscribe to a topic
   function onevent(args) {
      console.log("Event:", args[0]);
   }
   session.subscribe('com.myapp.hello', onevent);

   // 2) publish an event
   session.publish('com.myapp.hello', ['Hello, world!']);

   // 3) register a procedure for remoting
   function add2(args) {
      return args[0] + args[1];
   }
   session.register('com.myapp.add2', add2);

   // 4) call a remote procedure
   session.call('com.myapp.add2', [2, 3]).then(
      function (res) {
         console.log("Result:", res);
      }
   );
};

connection.open();

jQuery.ready(function () {

});
