var PROFILES_PAGE_DESTINATION = "settings.html";
var STORAGE_KEY = "AmazonMerchProfiles:v0.1";

var MENU_PAGE_URL_REGEX_TEST = "S*merch-page.html";
var MENU_PAGE_URL_REGEX = "S*\/add_details$";

var profiles = [];
var contextMenuItems = null;

var activeTabId = null,
    activeTabUrl = null;

init();

function createTestProfiles() {
   profiles = [
      {
         brandName: 'brand 1',
         title: 'title 1',
         price: '21,01',
         features: ['feature 11', 'feature 12'],
         description: 'description 1',
         uid: 1,
         enabled: true
      },
      {
         brandName: 'brand 2',
         title: 'title 2',
         price: '22,02',
         features: ['feature 21'],
         description: 'description 2',
         uid: 2,
         enabled: true
      },
      {
         brandName: 'brand 3',
         title: 'title 3',
         price: '23,03',
         features: [],
         description: 'description 3',
         uid: 3,
         enabled: true
      }
   ];
}

function init() {
   restoreProfilesFromStorage();
   setupRouter();
   setupActionButton();
   listenForTabsChanges();
   onInstalled();
}

function setupRouter() {
   chrome.runtime.onMessage.addListener(
      function (request, sender, sendResponse) {
         if (request.action === window.ampActions.getProfiles) {
            getProfiles(request, sender, sendResponse);
         }
         else if (request.action === window.ampActions.removeProfile) {
            removeProfile(request, sender, sendResponse);
         }
         else if (request.action === window.ampActions.importProfiles) {
            importProfiles(request, sender, sendResponse);
         }
         else if (request.action === window.ampActions.addProfile) {
            addProfile(request, sender, sendResponse);
         }
         else if (request.action === window.ampActions.changeArrayWithoutReload) {
            changeArrayWithoutReload(request, sender, sendResponse);
         }
         else if (request.action === window.ampActions.changeMenuMode) {
            changeMenuMode(request, sender, sendResponse);
         }
         else if (request.action === window.ampActions.changeArrayWithReload) {
            changeArrayWithReload(request, sender, sendResponse);
         }
         else if (request.action === window.ampActions.getProfilesFromKeyWatcher) {
            getProfilesFromKeyWatcher(request, sender, sendResponse);
         }
         else if (request.action === window.ampActions.refreshExtMenus) {
            refreshContextMenu(request, sender, sendResponse);
         }
      });
}

function refreshContextMenu(request, sender, sendResponse) {
    profiles = request.data;
    commitChanges();

    removeContextMenu();
    createContextMenu();
}

function onInstalled() {
   chrome.runtime.onInstalled.addListener(function (details) {
      if (details.reason == "install" || details.reason == "update") {
         chrome.windows.getAll({}, function (windows) {
            for (var i in windows)
               reloadWindow(windows[i]);
         });
      }
   });
}

function reloadWindow(win) {
   chrome.tabs.getAllInWindow(win.id, function (tabs) {
      for (var i in tabs) {
         var tab = tabs[i];
         if (new RegExp(MENU_PAGE_URL_REGEX).test(tab.url) || new RegExp(MENU_PAGE_URL_REGEX_TEST).test(tab.url)) {
            chrome.tabs.update(tab.id, {
               url: tab.url,
               selected: tab.selected
            }, null);
         }
      }
   });
}

function setupActionButton() {
   chrome.browserAction.onClicked.addListener(function (tab) {
      chrome.tabs.create({
         url: chrome.extension.getURL(PROFILES_PAGE_DESTINATION)
      });
   });
}

function listenForTabsChanges() {
   chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
      console.log('onUpdated', changeInfo.url)
      if (tabId === activeTabId && changeInfo.url) {
         activeTabUrl = changeInfo.url;
         isContextMenuRequired();
      }
   });

   chrome.tabs.onActivated.addListener(function (activeInfo) {
      
      activeTabId = activeInfo.tabId;

      chrome.tabs.get(activeTabId, function(tab) {
         activeTabUrl = tab.url;
         console.log('onActivated', activeTabUrl)
         isContextMenuRequired();
      });
   });
}

function restoreProfilesFromStorage() {
   var storageDataString = localStorage[STORAGE_KEY];

   if (storageDataString=='undefined' || (storageDataString===undefined)) {
      storageDataString = '[]';
   }

   if (storageDataString) {
      profiles = JSON.parse(storageDataString);
   }
   else {
      profiles = [];
   }
}

function commitChanges() {
   localStorage[STORAGE_KEY] = JSON.stringify(profiles);
}

function isContextMenuRequired() {
   console.log('isContextMenuRequired', activeTabUrl);
   if (new RegExp(MENU_PAGE_URL_REGEX).test(activeTabUrl) || new RegExp(MENU_PAGE_URL_REGEX_TEST).test(activeTabUrl)) {
      createContextMenu();
   }
   else {
      removeContextMenu();
   }
}

function createContextMenu() {
    var flag = JSON.parse(localStorage['AmazonMerchListing:v0.1']);

   console.log('createContextMenu', contextMenuItems);
   if (contextMenuItems) {
      return;
   }

   var menuOptions = {
      type: 'normal',
      title: 'Amazon Merch Profiles',
      contexts: ['page'],
      onclick: menuItemClicked
   };

    if (profiles.length==0) restoreProfilesFromStorage();

   var menuItemId = chrome.contextMenus.create(menuOptions, function () {
      console.log('callback');
      setTimeout(function () {
         console.log('timeout', menuItemId);
         contextMenuItems = {
            id: menuItemId,
            uid: 'root',
            subMenus: []
         };

         _.each(profiles, function (profile) {
            if (!profile.enabled) {
               return;
            }
            
            var subMenuOptions = {
               type: 'normal',
               title: flag ? profile.title : profile.brandName,
               contexts: ['page'],
               onclick: menuItemClicked,
               parentId: menuItemId
            };

            var subMenuItemId = chrome.contextMenus.create(subMenuOptions);
            contextMenuItems.subMenus.push({
               id: subMenuItemId,
               uid: profile.uid
            });
         });
      });
   });
}

function menuItemClicked(clickInfo) {
   if (!contextMenuItems || !contextMenuItems.subMenus) {
      return;
   }

   var menuItemId = clickInfo.menuItemId;
   console.log('clicked', menuItemId);
   var clickedProfile = _.find(contextMenuItems.subMenus, function (subMenu) {
      return subMenu.id === menuItemId;
   }) ;

   if (clickedProfile) {
      var clickedProfileInfo = _.find(profiles, function (profile) {
         return profile.uid === clickedProfile.uid;
      });

      if (clickedProfileInfo) {
         var messageToContentScript = {
            action: window.ampActions.prefillWithProfile,
            data: {
               profileInfo: clickedProfileInfo
            }
         };

         chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
            for (var i = 0; i < tabs.length; ++i) {
               chrome.tabs.sendMessage(tabs[i].id, messageToContentScript);
            }
         });
      }
   }
}

function removeContextMenu() {
    console.log('removeContextMenu');
   contextMenuItems = null;
   chrome.contextMenus.removeAll();
}

function sendProfilesToTheList() {
   var messageToContentScript = {
      action: window.ampActions.getProfilesResponse,
      data: profiles,
   };

   chrome.tabs.query({}, function (tabs) {
      for (var i = 0; i < tabs.length; ++i) {
         chrome.tabs.sendMessage(tabs[i].id, messageToContentScript);
      }
   });
}

function getProfiles(request, sender, sendResponse) {
   sendProfilesToTheList();
}

function removeProfile(request, sender, sendResponse) {
   var uid = request.data.uid;
   
   profiles = _.filter(profiles, function (profile) {
      return profile.uid != uid;
   });
   
   commitChanges();
   sendProfilesToTheList();
}

function importProfiles(request, sender, sendResponse) {
   profiles = request.data.profiles;
   commitChanges();
   sendProfilesToTheList();
}

function addProfile(request, sender, sendResponse) {
   var profile = request.data.profile;
   profiles.push(profile);
   commitChanges();
   sendProfilesToTheList();
}

function changeArrayWithoutReload(request, sender, sendResponse) {
   profiles = request.data.profiles;
   commitChanges();
}

function changeMenuMode(request, sender, sendResponse) {
    commitChanges();
}

function changeArrayWithReload(request, sender, sendResponse) {
   profiles = request.data.profiles;
   commitChanges();
   sendProfilesToTheList();
}

function getProfilesFromKeyWatcher() {
   var messageToContentScript = {
      action: window.ampActions.getProfilesFromKeyWatcherResponse,
      data: {
         profiles: profiles
      }
   };

   chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
      for (var i = 0; i < tabs.length; ++i) {
         chrome.tabs.sendMessage(tabs[i].id, messageToContentScript);
      }
   });
}
